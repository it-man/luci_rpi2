module("luci.tools.nwt", package.seeall)
local nfs   = require "nixio.fs"
local json = require "luci.jsonc"
function get_devicelist()
	local nwtconf = json.parse(nfs.readfile("/etc/nwt.json") or "")
	if type(nwtconf) == "table" and nwtconf.devices then
		return nwtconf.devices
	end
    return {}
end